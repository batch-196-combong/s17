
// Functions
	/*
	Syntax:
		function functionName () {
			code block statement
		}
	*/

function printName() {
	console.log('My name is David');
};

printName();

// function declaration vs expressions

declaredFunction();

function declaredFunction(){
	console.log("Hi! I am from declaredFunction")
}

declaredFunction();

// function expressions
	// anonymous function - function without a name

let variableFunction = function(){
	console.log('I am from variable function')
}

variableFunction()

let funcExpression = function funcName(){
	console.log('Hello from the other side')
}

funcExpression();

function declaredfunction () {
	console.log ("Hi I am from declared function")
}

// let declaredFunction = function() {
// 	console.log("Hi, I am from declared function")
// }
declaredFunction()

funcExpression = function(){
	console.log("Updated funcExpression")
}

funcExpression()

const constantFunction = function (){
	console.log('Initialized with const')
}

constantFunction();

// constantFunction = function(){
// 	console.log('cannot be reassigned')
// }

constantFunction();
//const cant be reassigned.

// Function Scoping
/*
	Javascript Variables has 3 types of scope:
	1. local/block scop
	2. global scope
	3. function scope
*/

{
	let localVar = 'Armando Perez'
}

let globalVar = "Mr. Worldwide"

console.log(globalVar)
// console.log(localVar) error since inside a local code block

function showNames(){
	// function Scoped variable
	var functionVar = 'Joe';
	const functionConst = 'Nick';
	let functionLet = 'Kevin'

	console.log(functionVar)
	console.log(functionConst)
	console.log(functionLet)
}

showNames()

// nested function

function myNewFunction (){
	let name='Yor'

	function nestedFunction(){
		let nestedName = 'Brando'
		console.log(nestedName)
	}
	nestedFunction()
}
myNewFunction();

// Function and Global Scoped Variable

// GLobal Scoped Variable
let globalName = "Allan";

function myNewFunction2(){
	let nameInside = "Marco"
	console.log(globalName)
}
myNewFunction2()


// alert
/*alert ('Hello')

function showAlert(){
	alert('Hello User')
};

showAlert ()*/

// prompt ()
/*
	Syntax:
		prompt()
*/

/*let samplePrompt = prompt('Enter Your Name')
console.log ('Hello ' + samplePrompt)

let sampleNullPrompt = prompt('Dont enter anything')
console.log(sampleNullPrompt)*/


function printWelcomeMessage(){
	let firstName = prompt('Enter your name')
	let lastName = prompt('enter last Name')

	console.log('Hello ' + firstName + ' ' + lastName)
}

printWelcomeMessage()

// Function Naming Conventions

function getCourses (){
	let courses = ['Science', 'Arithmetic', 'Grammar'];
	console.log(courses)
}

getCourses()